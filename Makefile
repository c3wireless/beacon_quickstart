SOURCES=$(wildcard *.md)
PDFS=$(SOURCES:.md=.pdf)

all: $(PDFS)

%.pdf: $(SOURCES)
	pandoc --toc --toc-depth=2 -Vcolorlinks=true --template template.tex $(@:.pdf=.md) -o $@
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH \
		-sOutputFile=$(@:.pdf=.ebook.pdf) $@

clean:
	rm -f *.pdf

live: $(PDFS)
	while /bin/true; do inotifywait $(SOURCES); make all; done
