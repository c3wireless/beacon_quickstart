% Beacon Quickstart for Android Developers
% C3 Wireless
% v0.2; 11 Jul. 2016

# iBeacon Overview

## Terminology

iBeacon
:     A lightweight wireless beacon protocol proposed by Apple. It
operates *only* over Bluetooth Low Energy.

UUID
:    A 16 byte value. It’s intended to be unique per organization

Major ID
:    A 2 byte value intended to represent an organizational unit
(e.g. Location, Region)

Minor ID
:    A 2 byte value intended to specify an individual beacon.

Transmission Power
:    A 1 byte value representing the expected RSSI value of the receiver
at a beacon distance of 1 m

Region
:   A concept in the Android Beacon library relating visibility of
beacons to the location of the Android device. Regions are denoted by
a filter that may optionally include any of UUID, Major and Minor.

Entering a Region
:   The device enters a region when it sees it's first beacon packet
from a beacon matching the Region (UUID, Major or Minor).

Leaving a Region
:   The device *leaves* a region when it has not seen a single beacon
packet from any beacon matching the region in a (configurable) timeout interval.


## Device Support

To use iBeacons with Android, Android >= 4.3 (API 18) is required and
the device must have a bluetooth chipset capable of Bluetooth
v4+. [According to Google](https://developer.android.com/about/dashboards/index.html) in July 2016 more than 78% of active
devices support API >= 18 (though some may not have BLE hardware).

## Data Available from Beacons

The simple packet format proposed by Apple supports only supports
UUID, Major ID, Minor ID.

It's intended that the combination of UUID, Major and Minor be
universally unique.

## Usage Strategy

The most common way to utilize beacon data is to filter beacons by
UUID, allowing only those from meaningful organizations and using
major/minor as a 32bit key into an existing datastore containing
relevant contextual data for the beacon.

# Adding Beacon Functions to Existing Application

An Android Studio example project is provided as a git repository with
*pre-beacon* (master) and *post-beacon* (beacon-support) branches. The
project is available at [Github](https://github.com/C3Wireless/MaintLog) 

This section of the paper will take you through the steps of adding
beacon functionality to the example application using the *ABL*
launching and filling fields in a sub-activity when near a beacon.

## Importing the example project

1. Open *Android Studio*

2. Select New $\rightarrow$ Project from Version Control $\rightarrow$
   Github
   
3. Enter the following for *Git Repository URL*:

	https://github.com/C3Wireless/MaintLog.git

## Adding the Android Beacon Library

The first task is to add the *Android Beacon Library* to the
project. The *Android Beacon Library* (ABL) is a high level library
that simplifies listening for and reacting upon sighting of many types
of beacons, including iBeacons.

1. Open build.gradle (Project: MaintenanceLog) file and ensure that
   `jcenter()` is included in the repositories section.
   
    ~~~ java
	// build.gradle (Project)
    buildscript {
	  ...
      repositories {
        jcenter()
	  }
	  ...
    }
	~~~

2. Open the `build.gradle` (Module: app) file and add the *ABL* to the
   dependencies section:

	~~~ java
	// build.gradle (Module: app)
	...
    dependencies {
	  compile 'org.altbeacon:android-beacon-library:2+'
	}
    ...
	~~~
	
## Enabling Bluetooth

The *Android Beacon Library* manifest includes `BLUETOOTH` and
`BLUETOOTH_ADMIN` privileges; so there is no need to add anything to
the application manifest, but the user may have disabled Bluetooth.

We need to ask the user to enable their Bluetooth adapter. This can be
accomplished by adding the following code to `.MainActivity.onCreate()`:

~~~ java
// Ask the user to enable Bluetooth if they have a BLE one
private static final int REQUEST_ENABLE_BT = 0;
...
protected void onCreate(Bundle b) {
  ...
  if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if (!mBluetoothAdapter.isEnabled()) {
      Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
      startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }
  } else {
    // Fire the callback anyway to consolidate permissions logic
    onActivityResult(REQUEST_ENABLE_BT, RESULT_OK, null);
  }
}
~~~

We must check that the user has agreed to the permissions we need to
setup beacon tracking. This is done by implementing the
`.MainActivity.onActivityResult` method:

~~~ java
private boolean BLUETOOTH_USABLE = false;
...
@Override
public void onActivityResult(int requestCode, int resultCode, Intent data) {
  if (requestCode == REQUEST_ENABLE_BT) {
    if (resultCode == RESULT_OK) {
	  Log.v(TAG, "Bluetooth adapter enabled successfully");
    } else {
      Log.v(TAG, "Failed to enable Bluetooth adapter");
    }
  }
}

~~~

## Asking for LOCATION Permission

However, for Android versions $\geq 6$ (Marshmallow), it's required to
ask for either `ACCESS_COARSE_LOCATION` or `ACCESS_FINE_LOCATION`
permissions in order to scan for beacons in the background.

There's no need to ask for this permission if Bluetooth isn't present
(or is disabled for some reason). We can therefore ask for the
additional permissions in the `.MainActivity.onActivityResult` method
once we are sure the Bluetooth adapter is squared away:

~~~ java
private static final int REQUEST_PERMISSION_LOCATION = 0;
private void initializeBeaconManager();
...
@Override
public void onActivityResult(int requestCode, int resultCode, Intent data) {
  if (requestCode == REQUEST_ENABLE_BT) {
    if (resultCode == RESULT_OK) {
      Log.v(TAG, "Bluetooth adapter enabled successfully");
      // Ask the user for COARSE_LOCATION if android >= M
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (this.checkSelfPermission(permission.ACCESS_COARSE_LOCATION) != 
		      PackageManager.PERMISSION_GRANTED) {
          requestPermissions(
		    new String[]{permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSION_LOCATION);
        } else {
		  // Already have permission
		  initializeBeaconManager();
      } else {
        // Don't need permission
		initializeBeaconManager();
      }
    } else {
      Log.v(TAG, "Failed to enable Bluetooth adapter");
    }
  }
}
~~~

Check that the user has granted the permission and signal the
application that Bluetooth is usable by implementing
`MainActivity.onRequestPermissionsResult` as follows:

~~~ java
@Override
public void onRequestPermissionsResult(int requestCode, String[] perms, int[] results) {
  if (requestCode == REQUEST_PERMISSION_LOCATION)  {
    int idx = Arrays.asList(perms).indexOf(permission.ACCESS_COARSE_LOCATION);
    if (results[idx] == PackageManager.PERMISSION_GRANTED) {
      initializeBeaconManager();
    }
  }
}
~~~

## Opening an Activity on Beacon Proximity

Edit the declaration for `MainActivity` to implement the
`BeaconConsumer` interface:

~~~ java
public class MainActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor>, BeaconConsumer {
~~~

Add some new class variables:

~~~ java
private BeaconManager beaconManager;
private Beacon lastBeacon = null;
private static final String BEACON_UUID = "ffffffff-ffff-ffff-ffff-ffffffffffff";
private static final int BEACON_MAJOR = 65535;
private Region MaintRegion = null;
private boolean beaconServiceStarted = false;
private static final float PROXIMITY_THRESHOLD_METERS = 1.0;
~~~

We track the lastBeacon seen, so that we don't immediately reopen an
activity for the same beacon immediately after it's been closed. For
most applications this is an insufficient heuristic.

`BEACON_UUID` and `BEACON_MAJOR` will be used to filter results to
only the beacons we care to track.

Here's a new private method that will bind a `BeaconManager` instance
to the Activity and configures it's parser to detect packets in
iBeacon layout. Other layouts are possible including some
[C3 Extensions to iBeacon](#c3-extensions-to-ibeacon). As a
side-effect, it will start the background *Beacon Service*:

~~~ java
private void initializeBeaconManager() {
  MaintRegion = new Region(
    "com.c3wireless.maintenancelog.region",
    Identifier.parse(BEACON_UUID),
    Identifier.fromInt(BEACON_MAJOR),
    null);
  beaconManager = BeaconManager.getInstanceForApplication(this);
  beaconManager.getBeaconParsers().add(
    new BeaconParser().setBeaconLayout(
      "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
      beaconManager.bind(this);
}
~~~

It's important to note the instantiation of Region. The concept of
Regions is central to the *Android Beacon Library*. Functionally, it
filters the results we'll be ranging to those including `BEACON_UUID`
and `BEACON_MAJOR` as defined above.

This is also where the beacon format is established. The value above
is for iBeacon. [See C3 Extensions](#c3-extensions-to-ibeacon) for a
packet format suitable for monitoring battery levels from C3 beacons.

Some modifications are needed to the `MainActivity` lifecycle methods:

~~~ java
@Override
protected void onDestroy() {
  super.onDestroy();
  if (beaconServiceStarted) {
    beaconManager.unbind(this);
  }
}

@Override
public void onPause() {
  ...
  if (beaconServiceStarted) {
    try {
      beaconManager.stopRangingBeaconsInRegion(MaintRegion);
    } catch (RemoteException e) {
      Log.v(TAG, "Failed to communicate with Beacon Service");
      System.exit(1);
    }
  }
  super.onPause();
}

@Override
public void onResume() {
  ...
  if (beaconServiceStarted) {
    try {
      beaconManager.startRangingBeaconsInRegion(MaintRegion);
    } catch (RemoteException e) {
      Log.v(TAG, "Failed to communicate with Beacon Service");
      System.exit(1);
    }
  }
}
~~~

### Ranging Lifecycle

As configured above, the Beacon Service will start at application
start (assuming we're granted the asked for permissions and the
hardware supports BLE). When the MainActivity is running the range to
all detected beacons in the `Region` (matching UUID/MAJOR) are
calculated.

Ranging is a very power hungry operation and should generally only be
performed when the relevant Activity is on-screen, so it's disabled in
the `onPause()` callback.

Finally, we need to start ranging and register a callback to receive
ranging events. After the *Beacon Service* is started (as a result of
`initializeBeaconService()`) the callback `onBeaconServiceConnect()`
is triggered. 

~~~ java
@Override
public void onBeaconServiceConnect() {
  // Set Callback for ranging to inline class
  beaconManager.setRangeNotifier(new RangeNotifier() {
  
    @Override
    public void didRangeBeaconsInRegion(
        Collection<Beacon> beacons, Region region) {
      for (Beacon b : beacons) {
        if (b.getDistance() <= PROXIMITY_THRESHOLD_METERS) {
          if (lastBeacon != null && b.getId3() != lastBeacon.getId3()) {
            lastBeacon = b;
		    // Launch intent using MINOR as primary key for equipment
            Intent intent = new Intent(mainActivityContext, AddLogActivity.class);
            intent.putExtra(AddLogActivity.EQUIPMENT_ID_KEY,
                            Long.valueOf(b.getId3().toInt()));
            startActivity(intent);
          }
        }
      }
    }
  });
  try {
    // Start ranging
    beaconManager.startRangingBeaconsInRegion(MaintRegion)
	beaconServiceStarted = true;
  } catch (RemoteException e) {
	Log.v(TAG, "Failed to communicate with Beacon Service");
	System.exit(1);
  }
}
~~~

Interesting things to note:

 - In this application we're using the `minor` number (`Beacon.id3()`)
   in the iBeacon packet as the key for the datastore.
   
    In a single site network of fewer that 65535 beacons this will
    work; however it'd be more common to hash major/minor (and
    possibly UUID) and use a separate unique key in the datastore.
   
 - We've set the proximity threshold for the `AddLogActivity` to open
   to \SI{1}{\meter}
   
 - Were passing the primary key to `AddLogActivity`, it has logic to
   fetch the record from the datastore to populate the form (same
   logic when the equipment is tapped in the `ListView`.
   
 - We must wait for the Beacon Service (the background agent) to start
   before we set the `RangeNotifier` and start ranging.

# C3 Extensions to iBeacon

C3 offers improvements over iBeacon protocol in the following areas:

## Geolocation of beacons

The iBeacon standard is predicated on beacons being stationary. Often
this will not be the case. If your organization wants to track moving
assets, C3 Wireless offers an iBeacon tracking solution.

The solution includes low-cost wireless and wired (PoE) tracking
stations. The system supplies beacon locations accurate to
\SI{1.5}{\meter} and with client specified projection for viewing with
GIS systems, Google Maps, &c.

## Security Beacons

C3 wireless also offers beacons that transmit Ephermeral IDs and are:

 - Impossible to spoof / replay packets
 - Drifting Keys offer clone detection guarentees
 - Hardended against (unauthorized) tracking of mobile beacons by
   Ephermeral IDs and MACs

## Beacon Battery Life Tracking

C3 beacons can be configured to transmit battery level
intermittently. Detection of the these battery level packets can be
had by adding an extra `BeaconParser` instance to the `BeaconManager`:

~~~ java
private void initializeBeaconManager {
  ...
  beaconManager.getBeaconParsers().add(
    new BeaconParser().setBeaconLayout(
      "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d=25"));
  ...
}
~~~

Upon monitoring or ranging a beacon, check the
`Beacon.getExtraDataFields()` any time there is an extra field, it's
battery data (0-100, in percent).
